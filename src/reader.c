#include "../include/queue.h"

void *reader(void *p_flow){
    /*init locals */
	int flow=*(int*)p_flow;
    unsigned int thread_id = (unsigned int)pthread_self();

int epoll_fd,
    epoll_ctl_error,
    epoll_wait_error;
struct epoll_event event_epoll;
    epoll_fd = epoll_create(1);                                            
    if(epoll_fd == -1){
        perror("epoll");
        return NULL;
    }
    
	/* fill event structure */
	event_epoll.events = EPOLLIN;
	event_epoll.data.fd = STDIN_FILENO;;
	/* add watch on particular descriptors */

	epoll_ctl_error = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, STDIN_FILENO, \
												&event_epoll);
	if(epoll_ctl_error == -1){
        perror("epoll_ctl");
        return NULL;
    }

	int i = 0;
    /* code */
	for( ;   i < 15 ; ++i ){

	epoll_wait_error = epoll_wait(epoll_fd, &event_epoll, 1, 0);
	if(epoll_wait_error == -1){
        perror("epoll_wait");
        return NULL;
    }
    if( epoll_wait_error ) return NULL;

		if ( pthread_mutex_lock(&no_writers) == 0 ) {
//            printf("Thread %d captured no_writers\n", (int)thread_id);
        }
		    pthread_mutex_lock(&counter_mutex);
    		    if ( readers_count == 0 ) pthread_mutex_lock(&no_readers);
		    readers_count++;
		    pthread_mutex_unlock(&counter_mutex);
		pthread_mutex_unlock(&no_writers);
 
//		printf("Thread %d can read\n", thread_id);
		printf("Thread %d checking if there is a message...\n", thread_id);
		write(flow, queue, strlen(queue));
		pthread_mutex_lock(&counter_mutex);
		    readers_count--;
		    if (readers_count == 0) {
                queue[0] = '\0';
                pthread_mutex_unlock(&no_readers);
            }
		pthread_mutex_unlock(&counter_mutex);
	}
    return NULL;
}
