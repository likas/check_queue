#include "../include/queue.h" 

int main(int argc, char** argv){	
    /* locals */ 
	int i;
	pthread_t t_r[READERS];
	pthread_t t_w[WRITERS];
    /* init locals */
    int file_fd,
        ptr_stdout = STDOUT_FILENO,
        ptr_stderr = STDERR_FILENO;
    queue[0] = '\0';
    file_fd = open("./file", O_CREAT | O_WRONLY, 0644);
    if(file_fd == -1){
        perror("open");
        return EXIT_FAILURE;
    }

    pthread_mutex_init(&no_writers, NULL);
    pthread_mutex_init(&no_readers, NULL);
    pthread_mutex_init(&counter_mutex, NULL);

    /* code */ 

	for(i = 0 ;i < WRITERS; ++i){
		pthread_create(&(t_w[i]),NULL,writer,(void*)&i);
	}

    sleep(3);

/*	for(i = 0; i < READERS; ++i){
 *		pthread_create(&(t_r[i]),NULL,reader,(void*)&i);
 *	} */
 	pthread_create(&(t_r[0]),NULL,reader,(void*)&(ptr_stderr));
 	pthread_create(&(t_r[1]),NULL,reader,(void*)&(ptr_stdout));
 	pthread_create(&(t_r[2]),NULL,reader,(void*)&file_fd);
 
	for(i = 0; i < READERS; ++i){
		pthread_join(t_r[i],NULL);
	}
	for(i = 0; i < WRITERS; ++i){
		pthread_join(t_w[i],NULL);
	}

    close(file_fd);

	pthread_mutex_destroy(&no_readers);
	pthread_mutex_destroy(&no_writers);
	pthread_mutex_destroy(&counter_mutex);
    return EXIT_SUCCESS;
}
