#include "../include/queue.h"

void *writer(void *none){
    /* init locals */
	pthread_t thread_id = pthread_self();
	int j = 0, timer;
    /* code */
    srand(time(NULL));
	for( ; j < MSG_COUNT; j++){

		if( pthread_mutex_lock(&no_writers) == 0 ) {
            /* printf("Thread %d accessing no_writers\n", (int)thread_id); */
        }
		pthread_mutex_lock(&no_readers);
		    pthread_mutex_unlock(&no_writers);
 
//		printf("Thread %d can (and will) write\n", (int)thread_id);
        sprintf(queue, "%d message from %d\n", j, (int)thread_id);
		timer = 1 + rand()%4;
		sleep(timer);
		pthread_mutex_unlock(&no_readers);
	}
    return NULL;
}
