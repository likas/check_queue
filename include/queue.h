#ifndef QUEUE_H
#define QUEUE_H

#include <pthread.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>
#include <sys/epoll.h>

#define WRITERS 1
#define READERS 3
#define MSG_COUNT 5

pthread_mutex_t no_writers;
pthread_mutex_t no_readers;
pthread_mutex_t counter_mutex;

char queue[128];
unsigned int readers_count;

void *reader(void *);
void *writer(void *);

#endif
